import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import LoginPage from "./screens/LoginPage";

import "./App.css";
import Home from "./screens/Home"
import ImageAvatars from "./screens/MinaSidor";
import Skola from "./screens/Skola"
import SkapaStudent from "./screens/SkapaStudent"
import SkapaArbetsgivare from "./screens/SkapaArbetsgivare"
import Dokument from "./screens/Dokument"
import LiaTinder from "./screens/LiaTinder"
import AllaLior from "./screens/AllLior";

function App() {

    return (
        <div className="App">
            <Router>
                {/* <Header /> */}
                <Switch>
                    <Route exact path="hej">
                        <a href="">
                            <link rel="stylesheet" href=""/>
                            <h2>shu bre</h2>
                        </a>
                        {/* <Login /> */}
                    </Route>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route exact path="/Skola">
                        <Skola/>
                    </Route>
                    <Route exact path="/Skola/SkapaStudent">
                        <SkapaStudent/>
                    </Route>
                    <Route exact path="/Skola/SkapaArbetsgivare">
                        <SkapaArbetsgivare/>
                    </Route>
                    <Route exact path="/Skola/Dokument">
                        <Dokument/>
                    </Route>
                    <Route exact path="/Skola/LiaTinder">
                        <LiaTinder/>
                    </Route>
                    <Route exact path="/login">
                        <LoginPage/>
                    </Route>
                    <Route exact path="/MinaSidor">
                        <ImageAvatars/>
                    </Route>
                    <Route exact path="/LiaMatch">
                        <AllaLior/>
                    </Route>

                </Switch>
            </Router>
        </div>
    );
}


export default App;