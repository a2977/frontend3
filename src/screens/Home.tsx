import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import { useHistory } from 'react-router-dom';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Astronauts.se
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    headerContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    materialButtons: {
        marginTop: theme.spacing(4),
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    dividerSpace: {
        marginTop: theme.spacing(30),
    },
}));

export default function Album() {
    const classes = useStyles();
    const history = useHistory();
    const studentLoginHandler = () => history.push('/login');

    return (
        <React.Fragment>
            <AppBar position="relative">
                <Toolbar>
                    <HomeIcon className={classes.icon}/>
                    <Typography variant="h5" color="inherit" align="center" style={{width: "100%"}}>
                        LIA PLATTFORM
                    </Typography>
                </Toolbar>
            </AppBar>

            <main>

                <div className={classes.headerContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Välkommen
                        </Typography>
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                            LIA är att betrakta som ett viktigt verktyg för att ge elever en praktisk tillämpning av
                            sina nyförvärvare kunskaper men det har visat sig också vara ett mycket bra sätt att för
                            elever att finna en framtida arbetsgivare i sin nya yrkesroll. För företagen kan det vara
                            ett bra och enkelt sätt att genomföra en "mindre provanställning" med minimala insatser till
                            ett mycket lågt pris.
                        </Typography>
                        <div className={classes.materialButtons}>
                            <Grid container spacing={2}
                                  direction="column"
                                  justifyContent="space-evenly"
                                  alignItems="stretch">
                                <Grid item>
                                    <Button onClick={studentLoginHandler} variant="contained" color="primary">
                                        Student Login
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button variant="contained" color="primary">
                                        Utbildare Login
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button variant="contained" color="primary">
                                        Företags Login
                                    </Button>
                                </Grid>
                            </Grid>
                        </div>
                    </Container>
                </div>
            </main>
            <Divider className={classes.dividerSpace}/>

            {/* Footer */}
            <footer className={classes.footer}>
                <Typography variant="h6" align="center" gutterBottom>
                    Footer
                </Typography>
                <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                    Footer text goes heeeeeeereeeeeeeeeee
                </Typography>
                <Copyright/>
            </footer>
            {/* End footer */}
        </React.Fragment>
    );
}