import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
  icon:{

  }
}));

export default function SignIn() {
    const classes = useStyles();
    const intialState = ["", "STUDENT", "TEACHER", "COMPANY"];
    const [loginDecider, setLoginDecider] = useState(intialState[0]);

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <HomeIcon className={classes.icon}/>
                    <Typography variant="h5" color="inherit" align="center" style={{width: "100%"}}>
                        Lia Tinder
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}