import * as React from "react";
import {
    DataGrid
} from "@material-ui/data-grid";
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import StarOutlinedIcon from '@material-ui/icons/StarOutlined';
enum Teknologier {
  Java = "java",
  Typescript = "Typescript",
  Php = "Php",
  Linux = "Linux",
  Javascript = "Javascript",
  C = "C",
  Apputveckling = "Apputveckling",
  Testare = "Testare",
  Python = "Python",
  NET = "NET",
}
const AllaLior = () => {



  return (
    <div style={{display:"block", height: 400, minWidth: "450px", width:"860px", marginLeft:"20%", marginRight:"50%"  }}>
      <DataGrid
        columns={[
          { field: "company",   width: 150 },
          { field: "email",    width: 180 },
          { field: "utbildningar",     width: 320 },
          {field: "favoriter", width:140}
        ]}
        rows={[
          {
            id: 1,
            company: "Volvo",
            email: "ribet@optonline.net",
            utbildningar: ["java", "linux", "php"],
            favoriter: true
          },
          {
            id: 2,
            company: "Google",
            email: "johndo@live.com",
            utbildningar: ["react", "typescript", "php"],


          },
          {
            id: 3,
            company: "NetonNet",
            email: "andersbr@yahoo.com",
            utbildningar: ["flutter", "typescript", "firebase"],
            favoriter: true


          },
          {
            id: 4,
            company: "MediaMarkt",
            email: "reziac@me.com",
            utbildningar: ["Kotlin", "tdd"],
            favoriter: true


          },
          {
            id: 5,
            company: "elgiganten",
            email: "moonlapse@aol.com",
            utbildningar: ["Redis", "Graphql"],
            favoriter: false


          },
          {
            id: 6,
            company: "Amazon",
            email: "mbswan@sbcglobal.net",
            utbildningar: ["Junit 5", "MongoDb"],
            favoriter: false


          },
          {
            id: 7,
            company: "Netflix",
            email: "floxy@icloud.com",
            favoriter: false

          },
        ]}
        pageSize={10}
        checkboxSelection
        disableSelectionOnClick
      />


      {/* {!checkboxSelection && return(
          
      )} */}
    </div>
  );
};
export default AllaLior;
